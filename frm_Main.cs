﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO.Ports;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Microsoft.Win32;
using System.Data.SqlClient;
using System.IO;
using DotNetSpeech;
using System.Configuration;


//using System.Speech.Synthesis;

namespace HFReader
{
    public partial class frm_Main : Form
    {
        public frm_Main()
        {
            InitializeComponent();
        }

        Int32 index = 0;
        //#region PrivateValues

    private ISO15693Reader reader = new ISO15693Reader();
          
      //  private ISO14443Reader reader = new ISO14443Reader();
      
        private delegate void AddListCallback(String str);
        private delegate void AddRowCallback(String framestr);
        private delegate void AddTagCallback();
        private delegate void AddResultCallback(String str);
        private List<String> CurrentTagNumbers;
        private Point theLocation = new Point(295, 6);
        private Point theLocation_1 = new Point(157, 6);
        //private SerialPort serialport;
        //private Boolean IsOpen = false;
        //private Thread threadRCV;
        private Thread threadAuto;
        private Thread threadMonitor;
        //private Byte CurrFrameNum = 0;
        //private Byte CurrCMD = 0x00;
        private String[] CurrUID = new String[0];
        private Boolean IsMonitor = false;
        private Boolean IsAutoRun = false;
        private Boolean IsRegistedEvent = false;
        //#endregion
        SpeechVoiceSpeakFlags spFlags = SpeechVoiceSpeakFlags.SVSFlagsAsync;
        SpVoice voice = new SpVoice();




        public Int32 GetComList(out List<String> ComList)
        {
            RegistryKey keyCom = Registry.LocalMachine.OpenSubKey("Hardware\\DeviceMap\\SerialComm");
            if (keyCom != null)
            {
                string[] sSubKeys = keyCom.GetValueNames();
                ComList = new List<string>();
                foreach (string sName in sSubKeys)
                {
                    ComList.Add((string)keyCom.GetValue(sName));
                }
                return ComList.Count;
            }
            else
            {
                ComList = null;
                return 0;
            }
        }

        private void AddInfoDemo(String str)
        {

            if (lst_Info_Demo.InvokeRequired)
            {
                AddListCallback d = new AddListCallback(AddInfoDemo);
                lst_Info_Demo.Invoke(d, str);
            }
            else
            {
                lst_Info_Demo.Items.Insert(0, String.Format("{0}:{1}", DateTime.Now.ToString(), str));
            }
        }

        private void Main_Load(object sender, EventArgs e)
        {


            cmb_BaudRate_Demo.SelectedIndex = 4;
            ///////////////////////////

            reader = new ISO15693Reader();


        }

        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            IsAutoRun = false;
            try
            {
                threadAuto.Abort();
                reader.CloseSerialPort();
            }
            catch { }
        }


        /// <summary>
        /// 将字节数组转换为十六进制的字符串
        /// </summary>
        /// <param name="array">字节数组</param>
        /// <returns>字符串</returns>
        private String ByteArrayToString(Byte[] array)
        {
            StringBuilder sb = new StringBuilder();
            foreach (Byte a in array) { sb = sb.Append(a.ToString("X2")); }
            return sb.ToString();
        }
        private String ByteArrayToString(Byte[] array, Int32 StartPos, Int32 Length)
        {
            StringBuilder sb = new StringBuilder();
            for (Int32 index = StartPos; index < StartPos + Length; index++)
            { sb = sb.Append(array[index].ToString("X2")); }
            return sb.ToString();
        }


        /// <summary>
        /// 将十六进制的字符串转换为字节数组
        /// </summary>
        /// <param name="str">字符串</param>
        /// <returns>字节数组</returns>
        private Byte[] StringToByteArray(String str)
        {
            Byte[] data = new Byte[str.Length / 2];
            for (Int32 i = 0; i < data.Length; i++)
            {
                data[i] = Convert.ToByte(str.Substring(i * 2, 2), 16);
            }
            return data;
        }



        private void btn_InventoryDemo_Click(object sender, EventArgs e)
        {
            if (!reader.IsOpen)
            {
                AddInfoDemo("请先打开串口。");
                return;
            }
            Int32 TagCount = 0x00;
            String[] TagNumbers = new String[0];
            Byte result = reader.Inventory(ModulateMethod.FSK, InventoryModel.Multiple, ref TagCount, ref TagNumbers);
            //Byte result = reader.GetAllTagsNum(out TagCount, out TagNumbers);
            if (result == 0x00)
            {
                AddInfoDemo(String.Format("Inventory命令执行成功，读取到的卡片数量为：{0}。", TagCount));
            }
            else
            {
                AddInfoDemo(String.Format("Inventory命令执行失败，失败代码{0}。", result));
                return;
            }
            if (TagCount > 0)
            {
                cmb_TagNumbers.Items.Clear();
                for (Int32 i = 0; i < TagNumbers.Length; i++)
                {
                    cmb_TagNumbers.Items.Add(TagNumbers[i]);
                }
                cmb_TagNumbers.SelectedIndex = 0;
            }
        }

        private void btn_Open_Demo_Click(object sender, EventArgs e)
        {
            if (!reader.IsOpen)
            {
                if (cmb_PortName_Demo.SelectedIndex < 0)
                {
                    AddInfoDemo(String.Format("错误：请您选择要打开的串口！"));
                    cmb_PortName_Demo.Focus();
                    return;
                }
                if (cmb_PortName_Demo.SelectedIndex < 0)
                {
                    AddInfoDemo(String.Format("错误：请您选择要使用的波特率！"));
                    cmb_PortName_Demo.Focus();
                    return;
                }
                String PortName = cmb_PortName_Demo.SelectedItem.ToString();
                Int32 BaudRate = 115200;// Int32.Parse(cmb_BaudRate.Text.Trim());
                Byte value = reader.OpenSerialPort(PortName, BaudRate);
                if (value == 0x00)
                {
                    AddInfoDemo(String.Format("串口{0}打开成功！", PortName));
                }
                else
                { AddInfoDemo(String.Format("错误：串口{0}打开失败！", PortName)); }
            }
            else
            {
                AddInfoDemo(String.Format("错误：串口已经处于打开状态！"));
            }
        }

        private void btn_Clear_Demo_Click(object sender, EventArgs e)
        {
            lst_Info_Demo.Items.Clear();
        }

        private void btn_Close_Demo_Click(object sender, EventArgs e)
        {
            if (reader.IsOpen)
            {
                Byte value = reader.CloseSerialPort();
                if (value == 0x00)
                    AddInfoDemo("串口关闭成功!");
                else
                    AddInfoDemo("串口关闭失败!");
            }
            else
            {
                AddInfoDemo(String.Format("错误：串口已经处于关闭状态！"));
            }
        }

        private void btn_Refresh_Demo_Click(object sender, EventArgs e)
        {

        }

        private void cmb_TagNumbers_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmb_TagNumbers.SelectedIndex >= 0)
            {
                SqlDataReader dr = SqlHelper.ExecuteReader(SqlHelper.CONSTRING, CommandType.Text, String.Format("Select * From tbl_UserInfo Where TagNumber='{0}'", cmb_TagNumbers.SelectedItem.ToString()));//connect database finish
                if (dr.Read())
                {
                    txt_UserName.Text = dr["UserName"].ToString();
                    txt_UserCompany.Text = dr["UserName"].ToString();
                    txt_UserTelephone.Text = dr["UserTelephone"].ToString();
                    txt_Appellative.Text = dr["UserAppellative"].ToString();
                    //if (dr["UserSex"].ToString().Trim().Equals("先生"))
                    //{ rb_Male.Checked = true; }
                    //else
                    //{ rb_Female.Checked = true; }
                    if (dr["UserPhoto"] != null)
                    {
                        try
                        {
                            Byte[] photo = (Byte[])dr[7];
                            if (photo.Length > 0)
                            {
                                System.IO.MemoryStream ms = new System.IO.MemoryStream(photo);
                                Bitmap bmp = new Bitmap(ms);
                                pic_UserPhoto.SizeMode = PictureBoxSizeMode.Zoom;
                                pic_UserPhoto.Image = bmp;
                            }
                            else
                            {
                                pic_UserPhoto.Image = null;
                            }
                        }
                        catch { pic_UserPhoto.Image = null; }
                    }
                    else
                    {
                        pic_UserPhoto.Image = null;
                    }
                    btn_Add.Enabled = false;
                    btn_Modify.Enabled = true;
                    btn_Delete.Enabled = true;
                }
                else
                {
                    txt_UserName.Clear();
                    txt_UserCompany.Clear();
                    txt_UserTelephone.Clear();
                    txt_Appellative.Clear();
                    //rb_Male.Checked = true;
                    pic_UserPhoto.Image = null;
                    pic_UserPhoto.ImageLocation = null;
                    btn_Add.Enabled = true;
                    btn_Modify.Enabled = false;
                    btn_Delete.Enabled = false;
                    txt_UserName.Focus();
                }
                dr.Close();
            }
            else
            {
                btn_Add.Enabled = false;
                btn_Modify.Enabled = false;
                btn_Delete.Enabled = false;
            }
        }

        private void btn_Add_Click(object sender, EventArgs e)
        {
            if (txt_UserName.Text.Trim().Length < 1)
            {
                AddInfoDemo("请填写员工姓名。");
                txt_UserName.Focus();
            }
            if (txt_UserCompany.Text.Trim().Length < 1)
            {
                AddInfoDemo("请填写所属部门。");
                txt_UserCompany.Focus();
            }
            if (txt_UserTelephone.Text.Trim().Length < 1)
            {
                AddInfoDemo("请填写联系电话。");
                txt_UserTelephone.Focus();
            }
            if (txt_Appellative.Text.Trim().Length < 1)
            {
                AddInfoDemo("请填写公司职位。");
                txt_Appellative.Focus();
            }
            SqlParameter[] sp;
            if (pic_UserPhoto.ImageLocation == null)
            {
                sp = new SqlParameter[]
                                { new SqlParameter("@TagNumber", SqlDbType.NVarChar, 50), 
                                  new SqlParameter("@UserName", SqlDbType.NVarChar,50),
                                  new SqlParameter("@UserCompany",SqlDbType.NVarChar,50),
                                  new SqlParameter("@UserTelephone", SqlDbType.NVarChar, 50),
                                  new SqlParameter("@UserAppellative", SqlDbType.NVarChar, 50)
                                    };
            }
            else
            {
                sp = new SqlParameter[]
                                { new SqlParameter("@TagNumber", SqlDbType.NVarChar, 50), 
                                  new SqlParameter("@UserName", SqlDbType.NVarChar,50),
                                  new SqlParameter("@UserCompany",SqlDbType.NVarChar,50),
                                  new SqlParameter("@UserTelephone", SqlDbType.NVarChar, 50),
                                  new SqlParameter("@UserAppellative", SqlDbType.NVarChar, 50),
                                  new SqlParameter("@UserPhoto", SqlDbType.Image)
                                    };
            }
            sp[0].Value = cmb_TagNumbers.SelectedItem.ToString().Trim();
            sp[1].Value = txt_UserName.Text.Trim();
            sp[2].Value = txt_UserCompany.Text.Trim();
            sp[3].Value = txt_UserTelephone.Text.Trim();
            sp[4].Value = txt_Appellative.Text.Trim();// rb_Male.Checked ? "先生" : "女士";
            if (pic_UserPhoto.ImageLocation != null)
            {
                Byte[] imagedata;
                using (FileStream fs = new FileStream(pic_UserPhoto.ImageLocation, FileMode.Open))
                {
                    imagedata = new Byte[fs.Length];
                    fs.Read(imagedata, 0, imagedata.Length);
                    fs.Close();
                }
                sp[5].Value = imagedata;
            }

            Int32 count;
            if (pic_UserPhoto.ImageLocation == null)
            {
                count = SqlHelper.ExecuteNonQuery(SqlHelper.CONSTRING, CommandType.Text,
                        "Insert Into tbl_UserInfo (TagNumber,UserName,UserCompany,UserTelephone,UserAppellative) Values(@TagNumber,@UserName,@UserCompany,@UserTelephone,@UserAppellative)", sp);
            }
            else
            {
                count = SqlHelper.ExecuteNonQuery(SqlHelper.CONSTRING, CommandType.Text,
"Insert Into tbl_UserInfo (TagNumber,UserName,UserCompany,UserTelephone,UserAppellative,UserPhoto) Values(@TagNumber,@UserName,@UserCompany,@UserTelephone,@UserAppellative,@UserPhoto)", sp);
            }
            if (count > 0)
            {
                AddInfoDemo("添加成功。");
                btn_InventoryDemo_Click(sender, e);
            }
            else
            {
                AddInfoDemo("添加失败。");
            }

        }

        private void btn_Modify_Click(object sender, EventArgs e)
        {
            if (cmb_TagNumbers.SelectedIndex < 0)
            { AddInfoDemo("请选择要修改的用户"); cmb_TagNumbers.Focus(); return; }
            if (txt_UserName.Text.Trim().Length < 1)
            {
                AddInfoDemo("请填写用户姓名。");
                txt_UserName.Focus();
            }
            if (txt_UserCompany.Text.Trim().Length < 1)
            {
                AddInfoDemo("请填写公司名称。");
                txt_UserCompany.Focus();
            }
            if (txt_UserTelephone.Text.Trim().Length < 1)
            {
                AddInfoDemo("请填写联系电话。");
                txt_UserTelephone.Focus();
            }
            if (txt_Appellative.Text.Trim().Length < 1)
            {
                AddInfoDemo("请填写习惯称谓。");
                txt_Appellative.Focus();
            }
            SqlParameter[] sp;

            if (pic_UserPhoto.ImageLocation == null)
            {
                sp = new SqlParameter[]
                                { new SqlParameter("@TagNumber", SqlDbType.NVarChar, 50), 
                                  new SqlParameter("@UserName", SqlDbType.NVarChar,50),
                                  new SqlParameter("@UserCompany",SqlDbType.NVarChar,50),
                                  new SqlParameter("@UserTelephone", SqlDbType.NVarChar, 50),
                                  new SqlParameter("@UserAppellative", SqlDbType.NVarChar, 50)
                                    };
            }
            else
            {
                sp = new SqlParameter[]
                                { new SqlParameter("@TagNumber", SqlDbType.NVarChar, 50), 
                                  new SqlParameter("@UserName", SqlDbType.NVarChar,50),
                                  new SqlParameter("@UserCompany",SqlDbType.NVarChar,50),
                                  new SqlParameter("@UserTelephone", SqlDbType.NVarChar, 50),
                                  new SqlParameter("@UserAppellative", SqlDbType.NVarChar, 50),
                                  new SqlParameter("@UserPhoto", SqlDbType.Image)
                                    };
            }
            sp[0].Value = cmb_TagNumbers.SelectedItem.ToString().Trim();
            sp[1].Value = txt_UserName.Text.Trim();
            sp[2].Value = txt_UserCompany.Text.Trim();
            sp[3].Value = txt_UserTelephone.Text.Trim();
            sp[4].Value = txt_Appellative.Text.Trim();// rb_Male.Checked ? "先生" : "女士";
            if (pic_UserPhoto.ImageLocation != null)
            {
                Byte[] imagedata;
                using (FileStream fs = new FileStream(pic_UserPhoto.ImageLocation, FileMode.Open))
                {
                    imagedata = new Byte[fs.Length];
                    fs.Read(imagedata, 0, imagedata.Length);
                    fs.Close();
                }
                sp[5].Value = imagedata;
            }

            Int32 count;
            if (pic_UserPhoto.ImageLocation == null)
            {
                count = SqlHelper.ExecuteNonQuery(SqlHelper.CONSTRING, CommandType.Text,
                        "Update tbl_UserInfo Set UserName=@UserName,UserCompany=@UserCompany,UserTelephone=@UserTelephone,UserAppellative=@UserAppellative Where TagNumber=@TagNumber", sp);
            }
            else
            {
                count = SqlHelper.ExecuteNonQuery(SqlHelper.CONSTRING, CommandType.Text,
                        "Update tbl_UserInfo Set UserName=@UserName,UserCompany=@UserCompany,UserTelephone=@UserTelephone,UserAppellative=@UserAppellative,UserPhoto=@UserPhoto Where TagNumber=@TagNumber", sp);
            }
            if (count > 0)
            {
                AddInfoDemo("修改成功。");
                btn_InventoryDemo_Click(sender, e);
            }
            else
            {
                AddInfoDemo("修改失败。");
            }
        }

        private void btn_Delete_Click(object sender, EventArgs e)
        {
            if (cmb_TagNumbers.SelectedIndex < 0)
            { AddInfoDemo("请选择要删除的用户"); cmb_TagNumbers.Focus(); return; }
            SqlParameter[] sp = { new SqlParameter("@TagNumber", SqlDbType.NVarChar, 50) };
            sp[0].Value = cmb_TagNumbers.SelectedItem.ToString().Trim();

            Int32 count;
            count = SqlHelper.ExecuteNonQuery(SqlHelper.CONSTRING, CommandType.Text,
                    "Delete From tbl_UserInfo Where TagNumber=@TagNumber", sp);
            if (count > 0)
            {
                AddInfoDemo("删除成功。");
                btn_InventoryDemo_Click(sender, e);
            }
            else
            {
                AddInfoDemo("删除失败。");
            }
        }

        private void btn_Browser_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.RestoreDirectory = true;
            //ofd.InitialDirectory = "|DataDirectory|";
            ofd.Filter = "*.jpg|*.jpg|*.gif|*.gif|*.bmp|*.bmp|*.png|*.png";
            ofd.CheckFileExists = true;
            ofd.Multiselect = false;
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                pic_UserPhoto.SizeMode = PictureBoxSizeMode.Zoom;
                pic_UserPhoto.ImageLocation = ofd.FileName;
            }
        }

        private void btn_Monitor_Click(object sender, EventArgs e)
        {
            if (btn_Monitor.Text.Trim().Equals("Start Monitor"))
            {
                if (!reader.IsOpen)
                {
                    AddInfoDemo("请先打开串口。");
                    return;
                }
                if (IsMonitor)
                    return;
                IsMonitor = true;
                // btn_Clear_Info_Click(sender, e);
                groupBox5.Enabled = false;
                threadMonitor = new Thread(new ThreadStart(Monitor));
                threadMonitor.IsBackground = true;
                threadMonitor.Start();
                btn_Monitor.Text = "Stop Monitor";
                AddInfoDemo("您启动了自动监控。");
            }
            else
            {
                IsMonitor = false;
                btn_Monitor.Text = "Start Monitor";
                groupBox5.Enabled = true;
                btn_Add.Enabled = false;
                btn_Modify.Enabled = false;
                btn_Delete.Enabled = false;
            }
        }

        private void Monitor()
        {
            Int32 TagCount = 0x00;
            String[] TagNumbers = new String[0];
            Byte result;
            CurrentTagNumbers = new List<string>();
            while (IsMonitor)
            {
                Thread.Sleep(500);
                result = reader.Inventory(ModulateMethod.FSK, InventoryModel.Multiple, ref TagCount, ref TagNumbers);
                //result = reader.GetAllTagsNum(out TagCount, out TagNumbers);
                if (result != 0x00)
                {
                    ClearInfo();
                    CurrentTagNumbers.Clear();
                    //AddInfoDemo(String.Format("GetAllTagsNum命令执行失败，失败代码{0}。", result));
                    continue;
                }
                if ((TagCount > 0) && (TagNumbers.Length > 0))
                {
                    foreach (String TagNumber in TagNumbers)
                    {
                        if (CurrentTagNumbers.IndexOf(TagNumber) < 0)
                        {
                            ShowInfo(TagNumber);
                        }
                    }
                    CurrentTagNumbers.Clear();
                    foreach (String TagNumber in TagNumbers)
                    {
                        CurrentTagNumbers.Add(TagNumber);
                    }
                }
                else
                {
                    ClearInfo();
                    CurrentTagNumbers.Clear();
                }
            }
            AddInfoDemo("您停止了自动监控。");
        }

        private void ClearInfo()
        {
            if (lst_Info_Demo.InvokeRequired)
            {
                AddTagCallback d = new AddTagCallback(ClearInfo);
                lst_Info_Demo.Invoke(d);
            }
            else
            {
                txt_UserName_Monitor.Clear();
                txt_TagNumber_Monitor.Clear();
                txt_Telephone_Monitor.Clear();
                txt_Company_Monitor.Clear();
                pic_Photo_Monitor.Image = null;
            }
        }

        private void ShowInfo(String TagNumber)
        {

            if (lst_Info_Demo.InvokeRequired)
            {
                AddListCallback d = new AddListCallback(ShowInfo);
                lst_Info_Demo.Invoke(d, TagNumber);
            }
            else
            {
                SqlDataReader dr = SqlHelper.ExecuteReader(SqlHelper.CONSTRING, CommandType.Text, String.Format("Select * From tbl_UserInfo Where TagNumber='{0}'", TagNumber));
                if (dr.Read())
                {
                    txt_TagNumber_Monitor.Text = TagNumber;
                    txt_Telephone_Monitor.Text = dr["UserTelephone"].ToString();
                    txt_UserName_Monitor.Text = dr["UserName"].ToString();
                    txt_Company_Monitor.Text = dr["UserCompany"].ToString();
                    lst_Info_Demo.Items.Insert(0, String.Format("{0}:{2}({1}) 进入监控区域。", DateTime.Now.ToString(), TagNumber, dr["UserName"].ToString()));
                    if (chk_Voice.Checked)
                    {
                        voice.Speak(String.Format("欢迎您，{0}。", dr["UserAppellative"].ToString().Trim()), spFlags);
                    }
                    if (dr["UserPhoto"] != null)
                    {
                        try
                        {
                            Byte[] photo = (Byte[])dr[7];
                            if (photo.Length > 0)
                            {
                                System.IO.MemoryStream ms = new System.IO.MemoryStream(photo);
                                Bitmap bmp = new Bitmap(ms);
                                pic_Photo_Monitor.SizeMode = PictureBoxSizeMode.Zoom;
                                pic_Photo_Monitor.Image = bmp;
                            }
                            else
                            {
                                pic_Photo_Monitor.SizeMode = PictureBoxSizeMode.Zoom;
                                pic_Photo_Monitor.ImageLocation = "nonephoto.jpg";
                            }
                        }
                        catch
                        {
                            pic_Photo_Monitor.SizeMode = PictureBoxSizeMode.Zoom;
                            pic_Photo_Monitor.ImageLocation = "nonephoto.jpg";
                        }
                    }
                    else
                    {
                        pic_Photo_Monitor.SizeMode = PictureBoxSizeMode.Zoom;
                        pic_Photo_Monitor.ImageLocation = "nonephoto.jpg";
                    }
                }
                else
                {
                    txt_TagNumber_Monitor.Text = TagNumber;
                    txt_Company_Monitor.Text = "无效卡";
                    txt_UserName_Monitor.Text = "无效卡";
                    txt_Telephone_Monitor.Text = "无效卡";
                    if (chk_Voice.Checked)
                    {
                        voice.Speak("请您出示有效证件。", spFlags);
                    }
                    pic_Photo_Monitor.SizeMode = PictureBoxSizeMode.Zoom;
                    pic_Photo_Monitor.ImageLocation = "invalid.jpg";
                    lst_Info_Demo.Items.Insert(0, String.Format("{0}:陌生人({1}) 进入监控区域。", DateTime.Now.ToString(), TagNumber));
                }
            }
        }

        private void txt_UserName_TextChanged(object sender, EventArgs e)
        {

        }

   
        private void chk_Voice_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void pic_Photo_Monitor_Click(object sender, EventArgs e)
        {

        }

        private void groupBox4_Enter(object sender, EventArgs e)
        {

        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            Form2 frm2 = new Form2();
            frm2.Show();
            this.Hide();
        }

        private void label85_Click(object sender, EventArgs e)
        {

        }

        private void groupBox6_Enter(object sender, EventArgs e)
        {

        }

        private void chk_Voice_CheckedChanged_1(object sender, EventArgs e)
        {

        }

        private void groupBox4_Enter_1(object sender, EventArgs e)
        {

        }

        private void txt_TagNumber_Monitor_TextChanged(object sender, EventArgs e)
        {

        }

        private void txt_UserName_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void lst_Info_Demo_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label89_Click(object sender, EventArgs e)
        {

        }










    }
}