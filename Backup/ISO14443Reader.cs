using System;
using System.Collections.Generic;
using System.Text;
using System.IO.Ports;

namespace HFReader
{
        class ISO14443Reader
        {
                #region PrivateProperty
                private SerialPort serialport;
                //private Socket socket;
                private Byte CurrNum = 0x00;
                //private Boolean isAutoRcv = false;             //用于AutoReceive
                //private Boolean isRegistedEvent = false;   //用于AutoReceive
                //private Int32 autoRcvdTagCount = 0;
                //private String[] autoRcvdTagNum = new String[] { };

                private Byte AllDone = 0x00;
                private Byte SerialPortErr = 0x01;
                private Byte SendTimeOut = 0x02;
                private Byte RcvTimeOut = 0x03;
                private Byte FrameFormatErr = 0x04;
                private Byte CRCERR = 0x05;
                private Byte DataLengthERR = 0x06;
                private Byte ReturnFrameNumErr = 0x07;
                private Byte ParameterErr = 0x08;
                private Byte EventErr = 0x09;   //用于AutoReceive
                private Byte RequestErr = 0x11;
                private Byte AntiCollErr = 0x12;
                private Byte SelectErr = 0x13;
                private Byte ResetToReadyErr = 0x14;
                private Byte AuthenticationErr = 0x15;
                private Byte ReadErr = 0x16;
                private Byte WriteErr = 0x17;
                #endregion

                #region PublicProperty
                public Boolean IsOpen
                {
                        get
                        {
                                try { return serialport.IsOpen; }
                                catch { return false; }
                        }
                }
                //public String ConnectType
                //{
                //        get
                //        {
                //                try { if (serialport.IsOpen) return "SerialPort"; }
                //                catch { }
                //                try { if (socket.Connected) return "Network"; }
                //                catch { }
                //                return "None";
                //        }
                //}
                //public String PortName
                //{
                //        get
                //        {
                //                try { return serialport.PortName; }
                //                catch { return null; }
                //        }
                //}
                //public Boolean IsAutoRcv
                //{
                //        get
                //        {
                //                return isAutoRcv;
                //        }
                //}                  //用于AutoReceive

                #endregion
                //#region PublicEvent
                //public delegate void FrameSendDel(String Frame);
                //public event FrameSendDel OnFrameSend;
                //public delegate void FrameReceiveDel(String Frame);
                //public event FrameReceiveDel OnFrameReceive;
                //public delegate void TagDetectedDel(Int32 TagNumber, String[] TagUIDs);
                //public event TagDetectedDel OnTagDetected;
                //#endregion

                #region PrivateMethod
                /// <summary>
                /// 将字节数组转换为十六进制的字符串
                /// </summary>
                /// <param name="array">字节数组</param>
                /// <returns>字符串</returns>
                private String ByteArrayToString(Byte[] array)
                {
                        StringBuilder sb = new StringBuilder();
                        foreach (Byte a in array) { sb = sb.Append(a.ToString("X2")); }
                        return sb.ToString();
                }
                private String ByteArrayToString(Byte[] array, Int32 StartPos, Int32 Length)
                {
                        StringBuilder sb = new StringBuilder();
                        for (Int32 index = StartPos; index < StartPos + Length; index++)
                        { sb = sb.Append(array[index].ToString("X2")); }
                        return sb.ToString();
                }


                /// <summary>
                /// 将十六进制的字符串转换为字节数组
                /// </summary>
                /// <param name="str">字符串</param>
                /// <returns>字节数组</returns>
                private Byte[] StringToByteArray(String str)
                {
                        Byte[] data = new Byte[str.Length / 2];
                        for (Int32 i = 0; i < data.Length; i++)
                        {
                                data[i] = Convert.ToByte(str.Substring(i * 2, 2), 16);
                        }
                        return data;
                }

                private Byte SendAFrame(Byte Command, Byte[] data)
                {
                        //if (!IsOpen) { return SerialPortErr; }
                        Byte[] frame = new Byte[data.Length + 6];
                        frame[0] = CurrNum;
                        frame[1] = Command;
                        frame[2] = (Byte)(data.Length * 2 % 256);
                        frame[3] = (Byte)(data.Length * 2 / 256);
                        for (Int32 i = 0; i < data.Length; i++)
                        {
                                frame[4 + i] += data[i];
                        }
                        CalculateCRC(ref frame, 0, data.Length + 4);
                        String strframe = ":";
                        strframe += ByteArrayToString(frame);
                        strframe += "\r\n";
                        CurrNum++;
                        try
                        {
                                serialport.DiscardInBuffer();
                                serialport.Write(strframe);
                                //OnFrameReceive(strframe);
                                return AllDone;
                        }
                        catch
                        {
                                return SendTimeOut;
                        }
                }

                private Byte RcvAFrame(ref Byte ReturnFrameNum, ref Byte StateCode, ref Int32 DataLength, ref Byte[] FrameData)
                {
                        //if (!IsOpen) { return SerialPortErr; }
                        String strFrame;
                        try
                        {
                                strFrame = serialport.ReadLine();
                        }
                        catch
                        {
                                return RcvTimeOut;
                        }
                        if (strFrame.IndexOf(":") != 0)
                        {
                                return FrameFormatErr; //需要改进改进
                                //return RcvAFrame(ref StateCode, ref FrameData, ref DataLength);
                        }
                        strFrame = strFrame.Remove(0, 1);
                        Byte[] Frame = new Byte[strFrame.Length / 2];
                        for (int i = 0; i < Frame.Length; i++)
                        { Frame[i] = Convert.ToByte(strFrame.Substring(i * 2, 2), 16); }
                        if (CheckCRC(Frame))
                        {
                                ReturnFrameNum = Frame[0];
                                if (ReturnFrameNum >= CurrNum) { return ReturnFrameNum; }
                                if (ReturnFrameNum < (CurrNum - 1)) { return RcvAFrame(ref ReturnFrameNum, ref StateCode, ref DataLength, ref FrameData); }
                                StateCode = Frame[1];
                                DataLength = Frame[3] * 256 + Frame[2];
                                if (DataLength != (Frame.Length - 6))
                                { return DataLengthERR; }
                                if (DataLength > 0)
                                {
                                        FrameData = new Byte[DataLength];
                                        for (Int32 i = 0; i < DataLength; i++)
                                        {
                                                FrameData[i] = Frame[4 + i];
                                        }
                                }
                                return AllDone;
                        }
                        else
                        { return CRCERR; }
                }

                private void CalculateCRC(ref Byte[] frame, Int32 offset, Int32 datalength)
                {
                        UInt16 crc16_preset = 0xFFFF;
                        UInt16 RFCRC16_POLYNOM = 0x8408;
                        UInt16 CRC16, m;
                        Byte n;
                        CRC16 = crc16_preset;
                        for (m = 0; m < datalength; m++)
                        {
                                CRC16 ^= (UInt16)(frame[offset + m] & 0xff);
                                //pointer++;

                                for (n = 0; n < 8; n++)
                                {
                                        if ((CRC16 & 0x0001) > 0)
                                                CRC16 = (UInt16)((CRC16 >> 1) ^ RFCRC16_POLYNOM);
                                        else
                                                CRC16 = (UInt16)(CRC16 >> 1);
                                }
                        }
                        frame[offset + datalength] = (Byte)(CRC16 % 256);
                        frame[offset + datalength + 1] = (Byte)(CRC16 / 256);
                }

                private Boolean CheckCRC(Byte[] frame)
                {
                        Int32 length = frame.Length;
                        Byte LowByte, UperByte;
                        UperByte = frame[length - 1];
                        LowByte = frame[length - 2];
                        CalculateCRC(ref frame, 0, length - 2);
                        if ((UperByte == frame[length - 1]) && (LowByte == frame[length - 2]))
                        { return true; }
                        else
                        { return false; }
                }

                #endregion

                #region PublicMethod

                /// <summary>
                /// 打开串口,参数portName为串口号,如"COM1",参数BaudRate为波特率，默认的模特率为115200;
                /// 其它串口参数采用默认设置:数据位8;停止位1;奇偶校验无.
                /// </summary>
                public Byte OpenSerialPort(String portName, Int32 BaudRate)
                {
                        return mOpenSerialPort(portName, BaudRate, 8, StopBits.One, Parity.None);
                }
                public Byte OpenSerialPort(String portName)
                {
                        return mOpenSerialPort(portName, 115200, 8, StopBits.One, Parity.None);
                }
                private Byte mOpenSerialPort(String portName, Int32 BaudRate, Int32 DataBits, StopBits StopBits, Parity Parity)
                {
                        try
                        {
                                serialport = new SerialPort(portName);
                                serialport.BaudRate = BaudRate;
                                serialport.DataBits = DataBits;
                                serialport.StopBits = StopBits;
                                serialport.Parity = Parity;
                                serialport.NewLine = "\r\n";
                                serialport.ReadTimeout = 3000;
                                serialport.Open();
                                if (serialport.IsOpen) return AllDone;  //打开成功
                                return SerialPortErr;  //打开失败
                        }
                        catch
                        {
                                return SerialPortErr; //打开失败
                        }
                }

                public Byte CloseSerialPort()
                {
                        try
                        {
                                serialport.Close();
                                if (!serialport.IsOpen)
                                {
                                        serialport = null;
                                        return AllDone;
                                }
                                else
                                {
                                        return SerialPortErr;
                                }
                        }
                        catch { return SerialPortErr; }
                }

                public Byte Request(Byte RequestFlag)
                {
                        if (!IsOpen) { return SerialPortErr; }
                        Byte command = 0x81;
                        Byte[] data = new Byte[1];
                        data[0] = RequestFlag;
                        Byte value = SendAFrame(command, data);
                        if (value != AllDone) { return value; }
                        Byte ReturnFrameNum = 0;
                        Byte StateCode = 0;
                        Int32 DataLength = 0;
                        Byte[] FrameData = new Byte[1];
                        value = RcvAFrame(ref ReturnFrameNum, ref  StateCode, ref DataLength, ref FrameData);
                        if (value != AllDone) { return value; }
                        if (StateCode != 0) { return RequestErr; }
                        if (DataLength == 0) { return RequestErr; }
                        if (FrameData[0] != 0) { return RequestErr; }
                        return AllDone;
                }

                public Byte AntiColl(Byte AntiCollFlag,out Byte[] TagUID)
                {
                        TagUID = new Byte[0];
                        if (!IsOpen) { return SerialPortErr; }
                        Byte command = 0x82;
                        Byte[] data = new Byte[1];
                        data[0] = AntiCollFlag;
                        Byte value = SendAFrame(command, data);
                        if (value != AllDone) { return value; }
                        Byte ReturnFrameNum = 0;
                        Byte StateCode = 0;
                        Int32 DataLength = 0;
                        Byte[] FrameData = new Byte[1];
                        value = RcvAFrame(ref ReturnFrameNum, ref  StateCode, ref DataLength, ref FrameData);
                        if (value != AllDone) { return value; }
                        if (StateCode != 0) { return AntiCollErr; }
                        if (DataLength == 0) { return AntiCollErr; }
                        if (FrameData[0] != 0) { return AntiCollErr; }
                        try
                        {

                                TagUID = new Byte[5];
                                for (Int32 i = 0; i < TagUID.Length; i++)
                                { TagUID[i] = FrameData[1 + i]; }
                                return AllDone;
                        }
                        catch
                        {
                                return AntiCollErr;
                        }
                }

                public Byte Select(Byte[] TagUID)
                {
                        if (!IsOpen) { return SerialPortErr; }
                        Byte command = 0x83;
                        //Byte[] data = new Byte[1];
                        //data[0] = AntiCollFlag;
                        Byte value = SendAFrame(command, TagUID);
                        if (value != AllDone) { return value; }
                        Byte ReturnFrameNum = 0;
                        Byte StateCode = 0;
                        Int32 DataLength = 0;
                        Byte[] FrameData = new Byte[1];
                        value = RcvAFrame(ref ReturnFrameNum, ref  StateCode, ref DataLength, ref FrameData);
                        if (value != AllDone) { return value; }
                        if (StateCode != 0) { return SelectErr; }
                        if (DataLength == 0) { return SelectErr; }
                        if (FrameData[0] != 0) { return SelectErr; }
                        return AllDone;
                        //try
                        //{

                        //        TagUID = new Byte[5];
                        //        for (Int32 i = 0; i < TagUID.Length; i++)
                        //        { TagUID[i] = FrameData[1 + i]; }
                        //        return AllDone;
                        //}
                        //catch
                        //{
                        //        return InventoryErr;
                        //}
                }

                public Byte Authentication(Byte[] TagUID, Byte BlockAddr, Byte[] Password)
                {
                        if (!IsOpen) { return SerialPortErr; }
                        Byte command = 0x84;
                        Byte[] data = new Byte[12];
                        try
                        {
                                for (Int32 i = 0; i < 5; i++)
                                {
                                        data[i] = TagUID[i];
                                }
                                data[5] = BlockAddr;
                                for (Int32 i = 0; i < 6; i++)
                                {
                                        data[6 + i] = Password[i];
                                }
                        }
                        catch
                        { return AuthenticationErr; }
                        //data[0] = AntiCollFlag;
                        Byte value = SendAFrame(command, data);
                        if (value != AllDone) { return value; }
                        Byte ReturnFrameNum = 0;
                        Byte StateCode = 0;
                        Int32 DataLength = 0;
                        Byte[] FrameData = new Byte[1];
                        value = RcvAFrame(ref ReturnFrameNum, ref  StateCode, ref DataLength, ref FrameData);
                        if (value != AllDone) { return value; }
                        if (StateCode != 0) { return AuthenticationErr; }
                        if (DataLength == 0) { return AuthenticationErr; }
                        if (FrameData[0] != 0) { return AuthenticationErr; }
                        return AllDone;
                        //try
                        //{

                        //        TagUID = new Byte[5];
                        //        for (Int32 i = 0; i < TagUID.Length; i++)
                        //        { TagUID[i] = FrameData[1 + i]; }
                        //        return AllDone;
                        //}
                        //catch
                        //{
                        //        return InventoryErr;
                        //}
                }

                public Byte Read(Byte BlockAddr, out Byte[] ReadData)
                {
                        ReadData = new Byte[0];
                        if (!IsOpen) { return SerialPortErr; }
                        Byte command = 0x85;
                        Byte[] data = new Byte[1];
                        data[0] = BlockAddr;
                        Byte value = SendAFrame(command, data);
                        if (value != AllDone) { return value; }
                        Byte ReturnFrameNum = 0;
                        Byte StateCode = 0;
                        Int32 DataLength = 0;
                        Byte[] FrameData = new Byte[1];
                        value = RcvAFrame(ref ReturnFrameNum, ref  StateCode, ref DataLength, ref FrameData);
                        if (value != AllDone) { return value; }
                        if (StateCode != 0) { return ReadErr; }
                        if (DataLength == 0) { return ReadErr; }
                        if (FrameData[0] != 0) { return ReadErr; }
                        try
                        {

                                ReadData = new Byte[16];
                                for (Int32 i = 0; i < ReadData.Length; i++)
                                { ReadData[i] = FrameData[1 + i]; }
                                return AllDone;
                        }
                        catch
                        {
                                return ReadErr;
                        }
                }

                public Byte Write(Byte BlockAddr, Byte[] WriteData)
                {
                        if (!IsOpen) { return SerialPortErr; }
                        Byte command = 0x86;
                        Byte[] data = new Byte[1 + WriteData.Length];
                        data[0] = BlockAddr;
                        for (Int32 i = 0; i < WriteData.Length; i++)
                        {
                                data[1 + i] = WriteData[i];
                        }
                        //data[0] = AntiCollFlag;
                        Byte value = SendAFrame(command, data);
                        if (value != AllDone) { return value; }
                        Byte ReturnFrameNum = 0;
                        Byte StateCode = 0;
                        Int32 DataLength = 0;
                        Byte[] FrameData = new Byte[1];
                        value = RcvAFrame(ref ReturnFrameNum, ref  StateCode, ref DataLength, ref FrameData);
                        if (value != AllDone) { return value; }
                        if (StateCode != 0) { return WriteErr; }
                        if (DataLength == 0) { return WriteErr; }
                        if (FrameData[0] != 0) { return WriteErr; }
                        return AllDone;
                        //try
                        //{

                        //        TagUID = new Byte[5];
                        //        for (Int32 i = 0; i < TagUID.Length; i++)
                        //        { TagUID[i] = FrameData[1 + i]; }
                        //        return AllDone;
                        //}
                        //catch
                        //{
                        //        return InventoryErr;
                        //}
                }
                #endregion
        }
}
