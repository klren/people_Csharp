﻿namespace HFReader
{
    partial class frm_Main
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label78 = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.label79 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.btn_Clear_Demo = new System.Windows.Forms.Button();
            this.lst_Info_Demo = new System.Windows.Forms.ListBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.chk_Voice = new System.Windows.Forms.CheckBox();
            this.txt_TagNumber_Monitor = new System.Windows.Forms.TextBox();
            this.pic_Photo_Monitor = new System.Windows.Forms.PictureBox();
            this.btn_Monitor = new System.Windows.Forms.Button();
            this.txt_Company_Monitor = new System.Windows.Forms.TextBox();
            this.label84 = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this.txt_Telephone_Monitor = new System.Windows.Forms.TextBox();
            this.label86 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.txt_UserName_Monitor = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.txt_Appellative = new System.Windows.Forms.TextBox();
            this.label89 = new System.Windows.Forms.Label();
            this.btn_Clear_Info = new System.Windows.Forms.Button();
            this.btn_Delete = new System.Windows.Forms.Button();
            this.btn_Add = new System.Windows.Forms.Button();
            this.btn_Modify = new System.Windows.Forms.Button();
            this.btn_Browser = new System.Windows.Forms.Button();
            this.pic_UserPhoto = new System.Windows.Forms.PictureBox();
            this.txt_UserTelephone = new System.Windows.Forms.TextBox();
            this.txt_UserCompany = new System.Windows.Forms.TextBox();
            this.txt_UserName = new System.Windows.Forms.TextBox();
            this.label88 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.btn_InventoryDemo = new System.Windows.Forms.Button();
            this.cmb_TagNumbers = new System.Windows.Forms.ComboBox();
            this.label82 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.button4 = new System.Windows.Forms.Button();
            this.btn_Refresh_Demo = new System.Windows.Forms.Button();
            this.btn_Close_Demo = new System.Windows.Forms.Button();
            this.btn_Open_Demo = new System.Windows.Forms.Button();
            this.cmb_BaudRate_Demo = new System.Windows.Forms.ComboBox();
            this.label80 = new System.Windows.Forms.Label();
            this.cmb_PortName_Demo = new System.Windows.Forms.ComboBox();
            this.label81 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage3.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Photo_Monitor)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_UserPhoto)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Interval = 3000;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(66, 107);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(60, 23);
            this.button1.TabIndex = 7;
            this.button1.Text = "Refresh";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(66, 77);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(60, 23);
            this.button2.TabIndex = 6;
            this.button2.Text = "Close";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(7, 77);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(53, 23);
            this.button3.TabIndex = 5;
            this.button3.Text = "Open";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "9600",
            "19200",
            "38400",
            "57600",
            "115200"});
            this.comboBox1.Location = new System.Drawing.Point(61, 49);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(65, 20);
            this.comboBox1.TabIndex = 4;
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(5, 57);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(53, 12);
            this.label78.TabIndex = 3;
            this.label78.Text = "BaudRate";
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "COM1",
            "COM2",
            "COM3",
            "COM4",
            "COM5",
            "COM6",
            "COM7",
            "COM8",
            "COM9",
            "COM10",
            "COM11",
            "COM12",
            "COM13",
            "COM14",
            "COM15",
            "COM16"});
            this.comboBox2.Location = new System.Drawing.Point(61, 21);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(65, 20);
            this.comboBox2.TabIndex = 2;
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(5, 29);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(53, 12);
            this.label79.TabIndex = 0;
            this.label79.Text = "PortName";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.btn_Clear_Demo);
            this.tabPage3.Controls.Add(this.lst_Info_Demo);
            this.tabPage3.Controls.Add(this.groupBox6);
            this.tabPage3.Controls.Add(this.groupBox5);
            this.tabPage3.Controls.Add(this.groupBox4);
            this.tabPage3.Location = new System.Drawing.Point(4, 26);
            this.tabPage3.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage3.Size = new System.Drawing.Size(1043, 478);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "员工录入与识别";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // btn_Clear_Demo
            // 
            this.btn_Clear_Demo.BackColor = System.Drawing.Color.LightBlue;
            this.btn_Clear_Demo.Location = new System.Drawing.Point(908, 448);
            this.btn_Clear_Demo.Margin = new System.Windows.Forms.Padding(4);
            this.btn_Clear_Demo.Name = "btn_Clear_Demo";
            this.btn_Clear_Demo.Size = new System.Drawing.Size(108, 30);
            this.btn_Clear_Demo.TabIndex = 8;
            this.btn_Clear_Demo.Text = "清除";
            this.btn_Clear_Demo.UseVisualStyleBackColor = false;
            this.btn_Clear_Demo.Click += new System.EventHandler(this.btn_Clear_Demo_Click);
            // 
            // lst_Info_Demo
            // 
            this.lst_Info_Demo.BackColor = System.Drawing.SystemColors.Info;
            this.lst_Info_Demo.FormattingEnabled = true;
            this.lst_Info_Demo.ItemHeight = 16;
            this.lst_Info_Demo.Location = new System.Drawing.Point(8, 285);
            this.lst_Info_Demo.Margin = new System.Windows.Forms.Padding(4);
            this.lst_Info_Demo.Name = "lst_Info_Demo";
            this.lst_Info_Demo.Size = new System.Drawing.Size(1008, 196);
            this.lst_Info_Demo.TabIndex = 45;
            this.lst_Info_Demo.SelectedIndexChanged += new System.EventHandler(this.lst_Info_Demo_SelectedIndexChanged);
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.DarkTurquoise;
            this.groupBox6.Controls.Add(this.chk_Voice);
            this.groupBox6.Controls.Add(this.txt_TagNumber_Monitor);
            this.groupBox6.Controls.Add(this.pic_Photo_Monitor);
            this.groupBox6.Controls.Add(this.btn_Monitor);
            this.groupBox6.Controls.Add(this.txt_Company_Monitor);
            this.groupBox6.Controls.Add(this.label84);
            this.groupBox6.Controls.Add(this.label85);
            this.groupBox6.Controls.Add(this.txt_Telephone_Monitor);
            this.groupBox6.Controls.Add(this.label86);
            this.groupBox6.Controls.Add(this.label90);
            this.groupBox6.Controls.Add(this.txt_UserName_Monitor);
            this.groupBox6.Location = new System.Drawing.Point(618, 8);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox6.Size = new System.Drawing.Size(398, 269);
            this.groupBox6.TabIndex = 44;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "员工信息识别";
            this.groupBox6.Enter += new System.EventHandler(this.groupBox6_Enter);
            // 
            // chk_Voice
            // 
            this.chk_Voice.AutoSize = true;
            this.chk_Voice.Location = new System.Drawing.Point(158, 27);
            this.chk_Voice.Margin = new System.Windows.Forms.Padding(4);
            this.chk_Voice.Name = "chk_Voice";
            this.chk_Voice.Size = new System.Drawing.Size(59, 20);
            this.chk_Voice.TabIndex = 23;
            this.chk_Voice.Text = "语音";
            this.chk_Voice.UseVisualStyleBackColor = true;
            this.chk_Voice.CheckedChanged += new System.EventHandler(this.chk_Voice_CheckedChanged_1);
            // 
            // txt_TagNumber_Monitor
            // 
            this.txt_TagNumber_Monitor.Location = new System.Drawing.Point(96, 61);
            this.txt_TagNumber_Monitor.Margin = new System.Windows.Forms.Padding(4);
            this.txt_TagNumber_Monitor.Name = "txt_TagNumber_Monitor";
            this.txt_TagNumber_Monitor.ReadOnly = true;
            this.txt_TagNumber_Monitor.Size = new System.Drawing.Size(166, 26);
            this.txt_TagNumber_Monitor.TabIndex = 22;
            this.txt_TagNumber_Monitor.TextChanged += new System.EventHandler(this.txt_TagNumber_Monitor_TextChanged);
            // 
            // pic_Photo_Monitor
            // 
            this.pic_Photo_Monitor.Location = new System.Drawing.Point(270, 60);
            this.pic_Photo_Monitor.Margin = new System.Windows.Forms.Padding(4);
            this.pic_Photo_Monitor.Name = "pic_Photo_Monitor";
            this.pic_Photo_Monitor.Size = new System.Drawing.Size(116, 162);
            this.pic_Photo_Monitor.TabIndex = 21;
            this.pic_Photo_Monitor.TabStop = false;
            // 
            // btn_Monitor
            // 
            this.btn_Monitor.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btn_Monitor.Location = new System.Drawing.Point(18, 23);
            this.btn_Monitor.Margin = new System.Windows.Forms.Padding(4);
            this.btn_Monitor.Name = "btn_Monitor";
            this.btn_Monitor.Size = new System.Drawing.Size(133, 30);
            this.btn_Monitor.TabIndex = 21;
            this.btn_Monitor.Text = "开启自动读卡";
            this.btn_Monitor.UseVisualStyleBackColor = false;
            this.btn_Monitor.Click += new System.EventHandler(this.btn_Monitor_Click);
            // 
            // txt_Company_Monitor
            // 
            this.txt_Company_Monitor.Location = new System.Drawing.Point(96, 151);
            this.txt_Company_Monitor.Margin = new System.Windows.Forms.Padding(4);
            this.txt_Company_Monitor.Name = "txt_Company_Monitor";
            this.txt_Company_Monitor.ReadOnly = true;
            this.txt_Company_Monitor.Size = new System.Drawing.Size(166, 26);
            this.txt_Company_Monitor.TabIndex = 11;
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Location = new System.Drawing.Point(18, 73);
            this.label84.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(72, 16);
            this.label84.TabIndex = 0;
            this.label84.Text = "编    号";
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Location = new System.Drawing.Point(18, 119);
            this.label85.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(72, 16);
            this.label85.TabIndex = 3;
            this.label85.Text = "员工姓名";
            this.label85.Click += new System.EventHandler(this.label85_Click);
            // 
            // txt_Telephone_Monitor
            // 
            this.txt_Telephone_Monitor.Location = new System.Drawing.Point(96, 195);
            this.txt_Telephone_Monitor.Margin = new System.Windows.Forms.Padding(4);
            this.txt_Telephone_Monitor.Name = "txt_Telephone_Monitor";
            this.txt_Telephone_Monitor.ReadOnly = true;
            this.txt_Telephone_Monitor.Size = new System.Drawing.Size(166, 26);
            this.txt_Telephone_Monitor.TabIndex = 15;
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Location = new System.Drawing.Point(18, 206);
            this.label86.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(72, 16);
            this.label86.TabIndex = 7;
            this.label86.Text = "联系电话";
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Location = new System.Drawing.Point(18, 163);
            this.label90.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(72, 16);
            this.label90.TabIndex = 8;
            this.label90.Text = "所属部门";
            // 
            // txt_UserName_Monitor
            // 
            this.txt_UserName_Monitor.Location = new System.Drawing.Point(96, 107);
            this.txt_UserName_Monitor.Margin = new System.Windows.Forms.Padding(4);
            this.txt_UserName_Monitor.Name = "txt_UserName_Monitor";
            this.txt_UserName_Monitor.ReadOnly = true;
            this.txt_UserName_Monitor.Size = new System.Drawing.Size(166, 26);
            this.txt_UserName_Monitor.TabIndex = 10;
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.MediumSlateBlue;
            this.groupBox5.Controls.Add(this.txt_Appellative);
            this.groupBox5.Controls.Add(this.label89);
            this.groupBox5.Controls.Add(this.btn_Clear_Info);
            this.groupBox5.Controls.Add(this.btn_Delete);
            this.groupBox5.Controls.Add(this.btn_Add);
            this.groupBox5.Controls.Add(this.btn_Modify);
            this.groupBox5.Controls.Add(this.btn_Browser);
            this.groupBox5.Controls.Add(this.pic_UserPhoto);
            this.groupBox5.Controls.Add(this.txt_UserTelephone);
            this.groupBox5.Controls.Add(this.txt_UserCompany);
            this.groupBox5.Controls.Add(this.txt_UserName);
            this.groupBox5.Controls.Add(this.label88);
            this.groupBox5.Controls.Add(this.label87);
            this.groupBox5.Controls.Add(this.label83);
            this.groupBox5.Controls.Add(this.btn_InventoryDemo);
            this.groupBox5.Controls.Add(this.cmb_TagNumbers);
            this.groupBox5.Controls.Add(this.label82);
            this.groupBox5.Location = new System.Drawing.Point(198, 8);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox5.Size = new System.Drawing.Size(412, 269);
            this.groupBox5.TabIndex = 44;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "员工信息录入";
            // 
            // txt_Appellative
            // 
            this.txt_Appellative.Location = new System.Drawing.Point(86, 168);
            this.txt_Appellative.Margin = new System.Windows.Forms.Padding(4);
            this.txt_Appellative.Name = "txt_Appellative";
            this.txt_Appellative.Size = new System.Drawing.Size(166, 26);
            this.txt_Appellative.TabIndex = 23;
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Location = new System.Drawing.Point(8, 178);
            this.label89.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(72, 16);
            this.label89.TabIndex = 22;
            this.label89.Text = "公司职位";
            this.label89.Click += new System.EventHandler(this.label89_Click);
            // 
            // btn_Clear_Info
            // 
            this.btn_Clear_Info.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btn_Clear_Info.Location = new System.Drawing.Point(248, 229);
            this.btn_Clear_Info.Margin = new System.Windows.Forms.Padding(4);
            this.btn_Clear_Info.Name = "btn_Clear_Info";
            this.btn_Clear_Info.Size = new System.Drawing.Size(68, 30);
            this.btn_Clear_Info.TabIndex = 21;
            this.btn_Clear_Info.Text = "清除";
            this.btn_Clear_Info.UseVisualStyleBackColor = false;
            // 
            // btn_Delete
            // 
            this.btn_Delete.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btn_Delete.Enabled = false;
            this.btn_Delete.Location = new System.Drawing.Point(157, 229);
            this.btn_Delete.Margin = new System.Windows.Forms.Padding(4);
            this.btn_Delete.Name = "btn_Delete";
            this.btn_Delete.Size = new System.Drawing.Size(83, 30);
            this.btn_Delete.TabIndex = 20;
            this.btn_Delete.Text = "删除";
            this.btn_Delete.UseVisualStyleBackColor = false;
            this.btn_Delete.Click += new System.EventHandler(this.btn_Delete_Click);
            // 
            // btn_Add
            // 
            this.btn_Add.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btn_Add.Enabled = false;
            this.btn_Add.Location = new System.Drawing.Point(8, 229);
            this.btn_Add.Margin = new System.Windows.Forms.Padding(4);
            this.btn_Add.Name = "btn_Add";
            this.btn_Add.Size = new System.Drawing.Size(58, 30);
            this.btn_Add.TabIndex = 19;
            this.btn_Add.Text = "添加";
            this.btn_Add.UseVisualStyleBackColor = false;
            this.btn_Add.Click += new System.EventHandler(this.btn_Add_Click);
            // 
            // btn_Modify
            // 
            this.btn_Modify.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btn_Modify.Enabled = false;
            this.btn_Modify.Location = new System.Drawing.Point(74, 229);
            this.btn_Modify.Margin = new System.Windows.Forms.Padding(4);
            this.btn_Modify.Name = "btn_Modify";
            this.btn_Modify.Size = new System.Drawing.Size(76, 30);
            this.btn_Modify.TabIndex = 18;
            this.btn_Modify.Text = "修改";
            this.btn_Modify.UseVisualStyleBackColor = false;
            this.btn_Modify.Click += new System.EventHandler(this.btn_Modify_Click);
            // 
            // btn_Browser
            // 
            this.btn_Browser.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btn_Browser.Location = new System.Drawing.Point(324, 229);
            this.btn_Browser.Margin = new System.Windows.Forms.Padding(4);
            this.btn_Browser.Name = "btn_Browser";
            this.btn_Browser.Size = new System.Drawing.Size(72, 30);
            this.btn_Browser.TabIndex = 17;
            this.btn_Browser.Text = "浏览";
            this.btn_Browser.UseVisualStyleBackColor = false;
            this.btn_Browser.Click += new System.EventHandler(this.btn_Browser_Click);
            // 
            // pic_UserPhoto
            // 
            this.pic_UserPhoto.Location = new System.Drawing.Point(262, 60);
            this.pic_UserPhoto.Margin = new System.Windows.Forms.Padding(4);
            this.pic_UserPhoto.Name = "pic_UserPhoto";
            this.pic_UserPhoto.Size = new System.Drawing.Size(133, 162);
            this.pic_UserPhoto.TabIndex = 16;
            this.pic_UserPhoto.TabStop = false;
            // 
            // txt_UserTelephone
            // 
            this.txt_UserTelephone.Location = new System.Drawing.Point(86, 132);
            this.txt_UserTelephone.Margin = new System.Windows.Forms.Padding(4);
            this.txt_UserTelephone.Name = "txt_UserTelephone";
            this.txt_UserTelephone.Size = new System.Drawing.Size(166, 26);
            this.txt_UserTelephone.TabIndex = 15;
            // 
            // txt_UserCompany
            // 
            this.txt_UserCompany.Location = new System.Drawing.Point(86, 96);
            this.txt_UserCompany.Margin = new System.Windows.Forms.Padding(4);
            this.txt_UserCompany.Name = "txt_UserCompany";
            this.txt_UserCompany.Size = new System.Drawing.Size(166, 26);
            this.txt_UserCompany.TabIndex = 11;
            // 
            // txt_UserName
            // 
            this.txt_UserName.Location = new System.Drawing.Point(86, 60);
            this.txt_UserName.Margin = new System.Windows.Forms.Padding(4);
            this.txt_UserName.Name = "txt_UserName";
            this.txt_UserName.Size = new System.Drawing.Size(166, 26);
            this.txt_UserName.TabIndex = 10;
            this.txt_UserName.TextChanged += new System.EventHandler(this.txt_UserName_TextChanged_1);
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(8, 107);
            this.label88.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(72, 16);
            this.label88.TabIndex = 8;
            this.label88.Text = "所属部门";
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Location = new System.Drawing.Point(8, 144);
            this.label87.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(72, 16);
            this.label87.TabIndex = 7;
            this.label87.Text = "联系电话";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(8, 72);
            this.label83.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(72, 16);
            this.label83.TabIndex = 3;
            this.label83.Text = "员工姓名";
            // 
            // btn_InventoryDemo
            // 
            this.btn_InventoryDemo.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btn_InventoryDemo.Location = new System.Drawing.Point(278, 23);
            this.btn_InventoryDemo.Margin = new System.Windows.Forms.Padding(4);
            this.btn_InventoryDemo.Name = "btn_InventoryDemo";
            this.btn_InventoryDemo.Size = new System.Drawing.Size(100, 30);
            this.btn_InventoryDemo.TabIndex = 2;
            this.btn_InventoryDemo.Text = "读卡";
            this.btn_InventoryDemo.UseVisualStyleBackColor = false;
            this.btn_InventoryDemo.Click += new System.EventHandler(this.btn_InventoryDemo_Click);
            // 
            // cmb_TagNumbers
            // 
            this.cmb_TagNumbers.FormattingEnabled = true;
            this.cmb_TagNumbers.Location = new System.Drawing.Point(86, 25);
            this.cmb_TagNumbers.Margin = new System.Windows.Forms.Padding(4);
            this.cmb_TagNumbers.Name = "cmb_TagNumbers";
            this.cmb_TagNumbers.Size = new System.Drawing.Size(183, 24);
            this.cmb_TagNumbers.TabIndex = 1;
            this.cmb_TagNumbers.SelectedIndexChanged += new System.EventHandler(this.cmb_TagNumbers_SelectedIndexChanged);
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(8, 36);
            this.label82.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(72, 16);
            this.label82.TabIndex = 0;
            this.label82.Text = "编    号";
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.LightGoldenrodYellow;
            this.groupBox4.Controls.Add(this.button4);
            this.groupBox4.Controls.Add(this.btn_Refresh_Demo);
            this.groupBox4.Controls.Add(this.btn_Close_Demo);
            this.groupBox4.Controls.Add(this.btn_Open_Demo);
            this.groupBox4.Controls.Add(this.cmb_BaudRate_Demo);
            this.groupBox4.Controls.Add(this.label80);
            this.groupBox4.Controls.Add(this.cmb_PortName_Demo);
            this.groupBox4.Controls.Add(this.label81);
            this.groupBox4.Location = new System.Drawing.Point(8, 8);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox4.Size = new System.Drawing.Size(181, 269);
            this.groupBox4.TabIndex = 43;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "串口设置";
            this.groupBox4.Enter += new System.EventHandler(this.groupBox4_Enter_1);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.LightSteelBlue;
            this.button4.Location = new System.Drawing.Point(89, 178);
            this.button4.Margin = new System.Windows.Forms.Padding(4);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(92, 30);
            this.button4.TabIndex = 8;
            this.button4.Text = "系统介绍";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click_1);
            // 
            // btn_Refresh_Demo
            // 
            this.btn_Refresh_Demo.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btn_Refresh_Demo.Location = new System.Drawing.Point(0, 178);
            this.btn_Refresh_Demo.Margin = new System.Windows.Forms.Padding(4);
            this.btn_Refresh_Demo.Name = "btn_Refresh_Demo";
            this.btn_Refresh_Demo.Size = new System.Drawing.Size(86, 30);
            this.btn_Refresh_Demo.TabIndex = 7;
            this.btn_Refresh_Demo.Text = "重置";
            this.btn_Refresh_Demo.UseVisualStyleBackColor = false;
            this.btn_Refresh_Demo.Click += new System.EventHandler(this.btn_Refresh_Demo_Click);
            // 
            // btn_Close_Demo
            // 
            this.btn_Close_Demo.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btn_Close_Demo.Location = new System.Drawing.Point(91, 112);
            this.btn_Close_Demo.Margin = new System.Windows.Forms.Padding(4);
            this.btn_Close_Demo.Name = "btn_Close_Demo";
            this.btn_Close_Demo.Size = new System.Drawing.Size(90, 30);
            this.btn_Close_Demo.TabIndex = 6;
            this.btn_Close_Demo.Text = "关闭";
            this.btn_Close_Demo.UseVisualStyleBackColor = false;
            this.btn_Close_Demo.Click += new System.EventHandler(this.btn_Close_Demo_Click);
            // 
            // btn_Open_Demo
            // 
            this.btn_Open_Demo.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btn_Open_Demo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_Open_Demo.Location = new System.Drawing.Point(0, 112);
            this.btn_Open_Demo.Margin = new System.Windows.Forms.Padding(4);
            this.btn_Open_Demo.Name = "btn_Open_Demo";
            this.btn_Open_Demo.Size = new System.Drawing.Size(86, 30);
            this.btn_Open_Demo.TabIndex = 5;
            this.btn_Open_Demo.Text = "打开";
            this.btn_Open_Demo.UseVisualStyleBackColor = false;
            this.btn_Open_Demo.Click += new System.EventHandler(this.btn_Open_Demo_Click);
            // 
            // cmb_BaudRate_Demo
            // 
            this.cmb_BaudRate_Demo.FormattingEnabled = true;
            this.cmb_BaudRate_Demo.Items.AddRange(new object[] {
            "9600",
            "19200",
            "38400",
            "57600",
            "115200"});
            this.cmb_BaudRate_Demo.Location = new System.Drawing.Point(82, 66);
            this.cmb_BaudRate_Demo.Margin = new System.Windows.Forms.Padding(4);
            this.cmb_BaudRate_Demo.Name = "cmb_BaudRate_Demo";
            this.cmb_BaudRate_Demo.Size = new System.Drawing.Size(86, 24);
            this.cmb_BaudRate_Demo.TabIndex = 4;
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(6, 76);
            this.label80.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(72, 16);
            this.label80.TabIndex = 3;
            this.label80.Text = "波 特 率";
            // 
            // cmb_PortName_Demo
            // 
            this.cmb_PortName_Demo.FormattingEnabled = true;
            this.cmb_PortName_Demo.Items.AddRange(new object[] {
            "COM1",
            "COM2",
            "COM3",
            "COM4",
            "COM5",
            "COM6",
            "COM7",
            "COM8",
            "COM9",
            "COM10",
            "COM11",
            "COM12",
            "COM13",
            "COM14",
            "COM15",
            "COM16"});
            this.cmb_PortName_Demo.Location = new System.Drawing.Point(82, 28);
            this.cmb_PortName_Demo.Margin = new System.Windows.Forms.Padding(4);
            this.cmb_PortName_Demo.Name = "cmb_PortName_Demo";
            this.cmb_PortName_Demo.Size = new System.Drawing.Size(86, 24);
            this.cmb_PortName_Demo.TabIndex = 2;
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(6, 39);
            this.label81.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(72, 16);
            this.label81.TabIndex = 0;
            this.label81.Text = "选择串口";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(5, 5);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1051, 508);
            this.tabControl1.TabIndex = 1;
            // 
            // frm_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.PowderBlue;
            this.ClientSize = new System.Drawing.Size(1026, 524);
            this.Controls.Add(this.tabControl1);
            this.Font = new System.Drawing.Font("SimSun", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ForeColor = System.Drawing.SystemColors.InfoText;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frm_Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "员工信息管理系统";
            this.Load += new System.EventHandler(this.Main_Load);
            this.tabPage3.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Photo_Monitor)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_UserPhoto)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer timer1;
            private System.Windows.Forms.Button button1;
            private System.Windows.Forms.Button button2;
            private System.Windows.Forms.Button button3;
            private System.Windows.Forms.ComboBox comboBox1;
            private System.Windows.Forms.Label label78;
            private System.Windows.Forms.ComboBox comboBox2;
            private System.Windows.Forms.Label label79;
            private System.Windows.Forms.TabPage tabPage3;
            private System.Windows.Forms.Button btn_Clear_Demo;
            private System.Windows.Forms.ListBox lst_Info_Demo;
            private System.Windows.Forms.GroupBox groupBox6;
            private System.Windows.Forms.CheckBox chk_Voice;
            private System.Windows.Forms.TextBox txt_TagNumber_Monitor;
            private System.Windows.Forms.PictureBox pic_Photo_Monitor;
            private System.Windows.Forms.Button btn_Monitor;
            private System.Windows.Forms.TextBox txt_Company_Monitor;
            private System.Windows.Forms.Label label84;
            private System.Windows.Forms.Label label85;
            private System.Windows.Forms.TextBox txt_Telephone_Monitor;
            private System.Windows.Forms.Label label86;
            private System.Windows.Forms.Label label90;
            private System.Windows.Forms.TextBox txt_UserName_Monitor;
            private System.Windows.Forms.GroupBox groupBox5;
            private System.Windows.Forms.TextBox txt_Appellative;
            private System.Windows.Forms.Label label89;
            private System.Windows.Forms.Button btn_Clear_Info;
            private System.Windows.Forms.Button btn_Delete;
            private System.Windows.Forms.Button btn_Add;
            private System.Windows.Forms.Button btn_Modify;
            private System.Windows.Forms.Button btn_Browser;
            private System.Windows.Forms.PictureBox pic_UserPhoto;
            private System.Windows.Forms.TextBox txt_UserTelephone;
            private System.Windows.Forms.TextBox txt_UserCompany;
            private System.Windows.Forms.TextBox txt_UserName;
            private System.Windows.Forms.Label label88;
            private System.Windows.Forms.Label label87;
            private System.Windows.Forms.Label label83;
            private System.Windows.Forms.Button btn_InventoryDemo;
            private System.Windows.Forms.ComboBox cmb_TagNumbers;
            private System.Windows.Forms.Label label82;
            private System.Windows.Forms.GroupBox groupBox4;
            private System.Windows.Forms.Button btn_Refresh_Demo;
            private System.Windows.Forms.Button btn_Close_Demo;
            private System.Windows.Forms.Button btn_Open_Demo;
            private System.Windows.Forms.ComboBox cmb_BaudRate_Demo;
            private System.Windows.Forms.Label label80;
            private System.Windows.Forms.ComboBox cmb_PortName_Demo;
            private System.Windows.Forms.Label label81;
            private System.Windows.Forms.TabControl tabControl1;
            private System.Windows.Forms.Button button4;

    }
}

